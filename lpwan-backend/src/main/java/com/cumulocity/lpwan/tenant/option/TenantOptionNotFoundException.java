package com.cumulocity.lpwan.tenant.option;

public class TenantOptionNotFoundException extends Exception {
    
    public TenantOptionNotFoundException(String message) {
        super(message);
    }

}
