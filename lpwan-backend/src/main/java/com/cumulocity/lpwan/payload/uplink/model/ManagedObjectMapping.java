package com.cumulocity.lpwan.payload.uplink.model;

import lombok.Data;

@Data
public class ManagedObjectMapping {

    private String fragmentType;
    
    private String innerType;

    
}
