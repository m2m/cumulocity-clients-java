package com.cumulocity.lpwan.payload.uplink.model;

import lombok.Data;

@Data
public class MeasurementMapping {
    
    private String type;
    
    private String series;
    

}
