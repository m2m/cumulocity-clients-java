package com.cumulocity.lpwan.payload.exception;

public class DeviceTypeObjectNotFoundException extends Exception {

    public DeviceTypeObjectNotFoundException(String message) {
        super(message);
    }
}
